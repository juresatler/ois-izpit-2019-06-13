/* global L, $ */

// seznam z markerji na mapi
var markerji = [];

var mapa;

const FRI_LAT = 46.05004;
const FRI_LNG = 14.46931;

/**
 * Ko se stran naloži, se izvedejo ukazi spodnje funkcije
 */
window.addEventListener('load', function () {
 var kolkoHistory=0;
  // Osnovne lastnosti mape
  var mapOptions = {
    center: [FRI_LAT, FRI_LNG],
    zoom: 9,
    maxZoom: 12
  };

  // Ustvarimo objekt mapa
  mapa = new L.map('mapa_id', mapOptions);

  // Ustvarimo prikazni sloj mape
  var layer = new L.TileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png');

  // Prikazni sloj dodamo na mapo
  mapa.addLayer(layer);

  // Akcija ob kliku na gumb za prikaz zgodovine
  $("#prikaziZgodovino").click(function() {
    window.location.href = "/zgodovina";
  });

  function obKlikuNaMapo(e) {
    // Izbriši prejšnje oznake
    for (var i = 0; i < markerji.length; i++) {
      mapa.removeLayer(markerji[i]);
    }

    var koordinate = e.latlng;

    // Dodaj trenutno oznako
    dodajMarker(koordinate.lat, koordinate.lng,
      "<b>Izbrana lokacija</b><br>(" + koordinate.lat + ", " + koordinate.lng + ")");
    //console.log($.get('https://api.lavbic.net/kraji/lokacija?lat='+koordinate.lat+'&lng='+koordinate.lng));
    //var hop = $.getJSON('https://api.lavbic.net/kraji/lokacija?lat='+koordinate.lat+'&lng='+koordinate.lng);
    //var nblPostna = hop.responseJSON[0].postnaStevilka;
    //var test = JSON.parse();
   /* var hop= $.get("/zabelezi-zadnji-kraj/:json", function(zahteva, odgovor) {
  var rezultat = zahteva.params.json;
  seznam_krajev.push(JSON.parse(rezultat));
  odgovor.send({steviloKrajev: seznam_krajev.length});
});
    */
  //var najblKraj=hop.responseJSON[0];
// var hop = $.get('https://api.lavbic.net/kraji/lokacija?lat='+koordinate.lat+'&lng='+koordinate.lng+'/:responseJSON');
  $.get('https://api.lavbic.net/kraji/lokacija?lat='+koordinate.lat+'&lng='+koordinate.lng+'/:responseJSON', function (zahteva, odgovor) {
  var hop = zahteva;
   console.log(hop);
   var najblKraj=hop[0];
   console.log(najblKraj);
  

   

    // Prikaži vremenske podatke najbližjega kraja
    $("#vremeOkolica").html("<p>Vreme v okolici kraja<br>"+najblKraj.kraj +"<br></p>");
 
    // Shrani podatke najbližjega kraja
    $.get('/zabelezi-zadnji-kraj/' + JSON.stringify({
      postnaStevilka: najblKraj.postnaStevilka,
      kraj: najblKraj.kraj,
      vreme: $.get('/vreme/:postnaStevilka'),
      temperatura: 10
    }), function(t) {
      kolkoHistory++;
      $("#prikaziZgodovino").html("Prikaži zgodovino "+kolkoHistory+"."+" krajev");
    });

    // Prikaži seznam 5 najbližjih krajev
    $("#bliznjiKraji").html("<h3>Bližnji kraji</h3><p> <b>"+hop[0].postnaStevilka+" </b>"+hop[0].kraj+"<br> <b>"+hop[1].postnaStevilka+" </b>"+hop[1].kraj+"<br> <b>"+hop[2].postnaStevilka+" </b>"+hop[2].kraj+"<br>  <b>"+hop[3].postnaStevilka+" </b>"+hop[3].kraj+"<br>  <b>"+hop[4].postnaStevilka+" </b>"+hop[4].kraj+"<br></p>");
  });
  }

  mapa.on('click', obKlikuNaMapo);

  // Na začetku postavi lokacijo na FRI
  var FRIpoint = new L.LatLng(FRI_LAT, FRI_LNG);
  mapa.fireEvent('click', {
    latlng: FRIpoint,
    layerPoint: mapa.latLngToLayerPoint(FRIpoint),
    containerPoint: mapa.latLngToContainerPoint(FRIpoint)
  });
});


/**
 * Dodaj izbrano oznako na zemljevid na določenih GPS koordinatah
 *
 * @param lat zemljepisna širina
 * @param lng zemljepisna dolžina
 * @param opis sporočilo, ki se prikaže v oblačku
 */
function dodajMarker(lat, lng, opis) {
  var ikona = new L.Icon({
    iconUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' +
      'marker-icon-2x-blue.png',
    shadowUrl: 'https://teaching.lavbic.net/cdn/OIS/DN1/' +
      'marker-shadow.png',
    iconSize: [25, 41],
    iconAnchor: [12, 41],
    popupAnchor: [1, -34],
    shadowSize: [41, 41]
  });

  var marker = L.marker([lat, lng], {icon: ikona});
  marker.bindPopup("<div>" + opis + "</div>").openPopup();
  marker.addTo(mapa);
  markerji.push(marker);
}
